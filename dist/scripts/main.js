'use strict';

var $winH,
    $winW,
    back_offsetTop,
    currentScroll = 0,
    $scrolled = 0,
    scrollbar,
    scrollbarMinicart,
    scrollbarCheckout,
    scrollbarSale,
    isSafari;

var app = {

  config: {

    Scrollbar: window.Scrollbar,
    pageScrollBare: '#pageScrollBar',
    minicart: '#minicart',
    parallax_elem: '.parallax'

  },

  init: function () {

    app.custom.init();
    app.globalVariable.init();
    app.overflow.init();
    app.preloader.init();
    app.up_button.init();
    app.login.init();
    app.header.init();
    app.cart.init();
    app.checkout.init();
    app.scrollbar.init();
    app.parallax.init();
    app.minicart.init();
    app.mainscreen.init();
    app.cardinfo.init();
    app.catalog_filter.init();
    app.scrollbar.scrollAnimation();
    app.card.init();
    app.sorting_articles.init();
    app.sorting.init();
    app.slider.init();
    app.profile.init();
    app.pagination.init();
    app.video.init();
    app.faq.init();
    app.jobs.init();
    app.diagram();
    app.magnificPopup();
    app.data_mask();
    app.validate();
    app.custom_select.creat();

    $(window).on('resize', function () {

      app.globalVariable.init_resize();
      app.header.init_resize();
      app.store.init_resize();
      app.slider.init_resize();
      app.parallax.set_stage();
    }).trigger('resize');
  },

  custom: {
    init: function () {
      var isMobile = {
        iOS: function () {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        }
      };
      if (isMobile.iOS()) {
        $('body, html').addClass('apple-ios');
      }
    }
  },

  globalVariable: {

    init: function () {

      $winH = $(window).height();
      $winW = $(window).width();

      isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
    },

    init_resize: function () {

      $winH = $(window).height();
      $winW = $(window).width();

      back_offsetTop = $('.cardblog__back').length > 0 && $('.cardblog__back').offset().top;
    }

  },

  overflow: {

    init: function () {

      app.overflow.creat();
      app.overflow.stop_scroll();
    },

    creat: function () {

      $(app.config.pageScrollBare).prepend('<div class="overflow"></div>');

      var overflow = $('.overflow');

      overflow.on('click', function () {

        app.minicart.close();
        app.checkout.close();
      });
    },

    show: function () {
      $('.overflow').addClass('-show-overflow');
    },

    hide: function () {
      setTimeout(function () {
        $('.overflow').removeClass('-show-overflow');
      }, 300);
    },

    stop_scroll: function () {

      $('.overflow')[0].addEventListener('wheel', e => e.preventDefault()); // останавливаем скролл страницы
    }

  },

  preloader: {

    init: function () {

      $(window).on('load', function () {

        $('.preloader').fadeOut('0');
        $('.page').removeClass('-loading');

        setTimeout(function () {

          $('body').css('overflow', '');
        }, 1500);
      });
    }

  },

  up_button: {

    init: function () {

      app.up_button.go_up();
    },

    go_up: function () {

      $('.scroll-up-button').on('click', function (e) {
        e.preventDefault();

        scrollbar.scrollTo(0, 0, 600);
      });
    },

    show: function (scrolled) {

      var header_height = $('.header').outerHeight();

      if (scrolled > header_height) {

        $('.scroll-up-button').addClass('-active');
      } else {

        $('.scroll-up-button').removeClass('-active');
      }

      if (scrolled >= 155) {
        $('.catalog_filter_wrapper .catalog_filter').addClass('catalog_scrolled').css({
          'transform': 'translate3d(0px, ' + scrolled + 'px, 0px',
          'margin-top': '-155px'
        });
      } else {
        $('.catalog_filter_wrapper .catalog_filter').removeClass('catalog_scrolled').css({
          'transform': 'translate3d(0px, 0px, 0px',
          'margin-top': '0'
        });
      }
    }

  },

  login: {

    init: function () {

      app.login.sign();
      app.login.person();
      app.login.show_password();
      app.login.forget();
    },

    sign: function () {

      var goto_sign_up = $('.goto_sign-up'),
          goto_sign_in = $('.goto_sign-in');

      goto_sign_up.on('click', function () {

        $('.sign-in__content').removeClass('-active -fade');
        $('.sign-up').addClass('-active');

        setTimeout(function () {
          $('.sign-up').addClass('-fade');
        }, 10);
      });

      goto_sign_in.on('click', function () {

        $('.sign-up').removeClass('-active -fade');
        $('.sign-in__content').addClass('-active');

        setTimeout(function () {
          $('.sign-in__content').addClass('-fade');
        }, 10);
      });
    },

    person: function () {

      var person = $('.sign-up__person'),
          check = person.find('.sign-up__person_check'),
          legal = $('.sign-up__legal'),
          back = legal.find('.sign-up__legal_back'),
          next = legal.find('.sign-up__legal_next');

      check.on('click', function (e) {
        e.preventDefault();
        var $this = $(this);

        if (!$this.hasClass('-checked')) {

          check.removeClass('-checked');
          $this.addClass('-checked');

          if ($this.hasClass('-physical')) {

            $('.sign-up__legal').removeClass('-active -fade');
            $('.sign-up__physical').addClass('-active');
            setTimeout(function () {
              $('.sign-up__physical').addClass('-fade');
            }, 10);
          } else if ($this.hasClass('-legal')) {

            $('.sign-up__physical').removeClass('-active -fade');
            $('.sign-up__legal').addClass('-active');
            setTimeout(function () {
              $('.sign-up__legal').addClass('-fade');
            }, 10);
          }
        }
      });

      next.on('click', function (e) {
        e.preventDefault();

        var step = legal.find('.sign-up__step'),
            step_active = legal.find('.sign-up__step.-active'),
            input = step_active.find('.sign-up__input'),
            input_pass = step_active.find('.sign-up__input.-password');

        if (legal.hasClass('-step-1')) {

          input.each(function () {

            if ($(this).val().length < 3) {
              $(this).addClass('-error');
            } else {
              $(this).removeClass('-error');
            }

            if (input_pass.val().length < 6) {
              input_pass.addClass('-error');
            } else {
              input_pass.removeClass('-error');
            }
          });

          if (input.hasClass('-error')) return;

          legal.removeClass('-step-1');
          step.removeClass('-active -fade');
          step.eq(1).addClass('-active');
          setTimeout(function () {
            step.eq(1).addClass('-fade');
            legal.addClass('-step-2');
          }, 10);
        } else if (legal.hasClass('-step-2')) {

          input.each(function () {

            if ($(this).val().length < 3) {
              $(this).addClass('-error');
            } else {
              $(this).removeClass('-error');
            }
          });

          if (input.hasClass('-error')) return;

          legal.removeClass('-step-2');
          legal.addClass('-step-3');

          step.removeClass('-active -fade');
          step.eq(2).addClass('-active');
          setTimeout(function () {
            step.eq(2).addClass('-fade');
          }, 10);
        }
      });

      back.on('click', function (e) {
        e.preventDefault();

        var step = legal.find('.sign-up__step');

        if (legal.hasClass('-step-2')) {

          legal.removeClass('-step-2');
          legal.addClass('-step-1');

          step.removeClass('-active -fade');
          step.eq(0).addClass('-active');
          setTimeout(function () {
            step.eq(0).addClass('-fade');
          });
        } else if (legal.hasClass('-step-3')) {

          legal.removeClass('-step-3');
          legal.addClass('-step-2');

          step.removeClass('-active -fade');
          step.eq(1).addClass('-active');
          setTimeout(function () {
            step.eq(1).addClass('-fade');
          });
        }
      });
    },

    show_password: function () {

      $('input[name="password"]').on('change input', function () {

        var $this = $(this),
            password = $this.next('.show-password');

        if ($this.val().length > 0) {
          password.addClass('-visible');
        } else {
          password.removeClass('-visible');
        }
      });

      $('.show-password').on('click', function () {

        var $this = $(this),
            input = $this.prev('input');

        if (!$this.hasClass('-show')) {
          $this.addClass('-show');
          input.attr('type', 'text');
        } else {
          $this.removeClass('-show');
          input.attr('type', 'password');
        }
      });
    },

    forget: function () {

      $('.sign-in__forget').on('click', function (e) {
        e.preventDefault();

        $.magnificPopup.open({
          items: {
            type: 'inline',
            src: '#password-modal'
          },
          mainClass: 'mfp-fade',
          removalDelay: 300
        });
      });
    }

  },

  minicart: {

    init: function () {

      var minicart = $(app.config.minicart),
          minicart_close = $('#minicartClose'),
          minicart_delete = minicart.find('.minicart__delete'),
          minicart_add = $('.js-addMinicart');

      minicart_add.on('click', function (e) {

        if ($(window).width() > 768) {
          e.preventDefault();

          app.minicart.open();
        }
      });

      minicart_close.on('click', function (e) {
        e.preventDefault();

        app.minicart.close();
      });

      minicart_delete.on('click', function (e) {
        e.preventDefault();
        $(this).closest('.minicart__item').slideUp(500, function () {
          $(this).closest('.minicart__item').remove();
        });
      });
    },
    open: function () {
      $(app.config.minicart).addClass('-show-minicart');
      app.overflow.show();
      $('.header').slideUp();
    },
    close: function () {
      $(app.config.minicart).removeClass('-show-minicart');
      app.overflow.hide();
      $('.header').slideDown();
    }

  },

  cart: {

    init: function () {

      app.cart.delete_product();
    },

    delete_product: function () {

      $(document).on('click', '.cart__delete', function (e) {
        e.preventDefault();

        $(this).closest('.cart__item').slideUp(500, function () {
          $(this).closest('.cart__item').remove();
        });
      });
    }

  },

  checkout: {

    init: function () {

      app.checkout.open_checkout();
      app.checkout.shipping.change();
      app.checkout.shipping.map();
      app.checkout.slide();
      app.checkout.legal();
    },

    open_checkout: function () {

      var checkoutOrder = $('#checkoutOrder'),
          checkout_close = $('#checkoutClose');

      checkoutOrder.on('click', function (e) {
        e.preventDefault();

        app.checkout.open();
      });

      checkout_close.on('click', function (e) {
        e.preventDefault();
        app.checkout.close();
      });
    },
    open: function () {
      $('.checkout').addClass('-show-checkout');
      app.overflow.show();
      $('.header').slideUp();
    },
    close: function () {
      $('.checkout').removeClass('-show-checkout');
      app.overflow.hide();
      $('.header').slideDown();
    },
    fixed: function (scrolled) {

      $('.checkout').css('top', scrolled + 'px');
    },
    shipping: {

      change: function () {

        $(document).on('change', 'input[name="shipping_method"]', function () {

          var $this = $(this);

          $('.checkout__shipping_input').closest('.checkout__shipping_label').removeClass('-active').addClass('-deactive');
          $this.closest('.checkout__shipping_label').removeClass('-deactive').addClass('-active');

          if ($this.val() === 'shipping_rate') {
            $('.checkout__shipping_pickup-sdek').slideUp();
            $('input[name="shipping_sdek"]').prop('checked', false);
            $('.checkout__shipping_rate').slideUp(function () {
              $('.checkout__shipping_rate').slideDown();
            });
          } else {
            $('.checkout__shipping_rate').slideUp();
          }

          if ($this.val() === 'shipping_sdek') {
            $('.checkout__shipping_sdek').slideDown();
            $('.checkout__shipping_pickup-sdek').slideUp();
          } else {
            $('.checkout__shipping_sdek').slideUp();
          }

          if ($this.val() === 'shipping_pickup') {
            $('.checkout__shipping_pickup').slideDown();

            $('.checkout__shipping_pickup-sdek').slideUp();
            $('input[name="shipping_sdek"]').prop('checked', false);

            $.magnificPopup.open({
              items: {
                type: 'inline',
                src: '#map-modal'
              },
              mainClass: 'mfp-fade',
              removalDelay: 300
            });
          } else {
            $('.checkout__shipping_pickup').slideUp();
          }
        });

        $(document).on('change', 'input[name="shipping_sdek"]', function () {

          var $this = $(this);

          if ($this.val() === 'shipping_sdek_rate') {
            $('.checkout__shipping_pickup-sdek').slideUp(function () {
              $('.checkout__shipping_rate').slideDown();
            });
          }

          if ($this.val() === 'shipping_sdek_pickup') {
            $('.checkout__shipping_rate').slideUp(function () {
              $('.checkout__shipping_pickup-sdek').slideDown();
            });
          }
        });
      },

      map: function () {
        if ($('#map').length > 0) {
          $(window).on('load', function () {

            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 12,
              center: { lat: 55.789257, lng: 37.614520 }
            });

            var contentString = '<div class="map_content">' + '<div class="cafe_title">Кофейня 8/25</div>' + '<div class="cafe_address">Москва, ул. Большая Новодмитровская, 36с1</div>' + '<div class="cafe_schedule">Режим работы: пн-вск с 10 до 20</div>' + '<a href="javascript:;" data-address="Москва, ул. Большая Новодмитровская, 36с1" class="cafe_select">ВЫБРАТЬ ПУНКТ САМОВЫВОЗА</a>' + '</div>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            });

            var marker = new google.maps.Marker({
              position: { lat: 55.804902, lng: 37.5828355 },
              map: map
            });

            marker.addListener('click', function () {
              infowindow2.close();
              infowindow.open(map, marker);
            });

            var contentString2 = '<div class="map_content">' + '<div class="cafe_title">Кофейня в кинотеатре "Москино"</div>' + '<div class="cafe_address">Москва, ул. Земляной Вал, 18/22 , стр.2</div>' + '<div class="cafe_schedule">График работы Пн-Пт с 9 до 18</div>' + '<a href="javascript:;" data-address="Москва, ул. Земляной Вал, 18/22 , стр.2" class="cafe_select">ВЫБРАТЬ ПУНКТ САМОВЫВОЗА</a>' + '</div>';

            var infowindow2 = new google.maps.InfoWindow({
              content: contentString2
            });

            var marker2 = new google.maps.Marker({
              position: { lat: 55.7597895, lng: 37.6570246 },
              map: map
            });

            marker2.addListener('click', function () {
              infowindow.close();
              infowindow2.open(map, marker2);
            });
          });

          $(document).on('click', '.map_content .cafe_select', function () {
            var address = $(this).attr('data-address');
            $('input[name="shipping_address"]').val(address);
            $.magnificPopup.close({
              items: {
                type: 'inline',
                src: '#map-modal'
              }
            });
          });

          $('.jsChangePickupPoint').on('click', function () {
            $.magnificPopup.open({
              items: {
                type: 'inline',
                src: '#map-modal'
              },
              mainClass: 'mfp-fade',
              removalDelay: 300
            });
          });
        }
      }

    },
    legal: function () {

      $(document).on('click', 'input[name="payment_method"]', function () {

        if ($(this).val() === 'payment_cashless') {
          $('.checkout__legal').slideDown();
        } else {
          $('.checkout__legal').slideUp();
        }
      });
    },
    slide: function () {

      $('.checkout__title.-slide').on('click', function () {

        if ($(window).width() < 768) {

          var $this = $(this),
              slide = $this.next('.checkout__slide');

          $this.toggleClass('-hide');
          slide.slideToggle();
        }
      });
    }

  },

  header: {

    init: function () {

      app.header.menu_toggle();
      app.header.background_creat();
      app.header.popup_phone();
      app.header.title_cliptext();
    },

    init_resize: function () {
      app.header.menu();
    },

    background_creat: function () {

      $('.header').append('<div class="header__bg"></div>');
    },

    popup_phone: function () {

      $('.mainicons__link--phone').on('click', function () {

        $(this).toggleClass('-hover');
      });

      $(document).on('click', function (e) {

        if (!$(e.target).closest('.mainicons__link--phone').length) {
          $('.mainicons__link--phone').removeClass('-hover');
        }
      });
    },

    background: function (scrolled) {

      var header = $('.header'),
          header_bg = header.find('.header__bg'),
          header_height = header.outerHeight();

      if (scrolled <= header_height) {
        header_bg.css('bottom', header_height - scrolled);
      } else {
        header_bg.css('bottom', 0);
      }

      if (scrolled > header_height) {
        header.addClass('-change-logo');
      } else {
        header.removeClass('-change-logo');
      }
    },

    fixed: function (scrolled) {

      // transform: translate3d(0px, -1336px, 0px);

      $('.header').css('transform', 'translate3d(0px, ' + scrolled + 'px, 0px');
      //$('.menu__submenu').css('top', scrolled + 'px');
    },

    hide_submenu: function () {
      $('.menu__item').removeClass('-show-submenu');
    },

    menu: function () {

      var window_width = $(window).width(),
          header = $('.header'),
          header_menu = header.find('.header__menu'),
          menu = header.find('.menu');

      if (window_width >= 1200) {

        if (header.find('.header__mobile').length) {

          // menu.prependTo(header_menu);
          // header.find('.header__mobile').remove();

        }
      } else {

          // if (header.find('.header__mobile').length) return;

          // var header_mobile = $('' +
          //     '<div class="header__mobile">' +
          //     '<div class="header__mobile_menu"></div>' +
          //     '</div>').appendTo(header);


          // var header_mobile = $('' +
          //     '<div class="header__mobile">' +
          //     '<div class="header__mobile_title">Меню</div>' +
          //     '<div class="header__mobile_menu"></div>' +
          //     '</div>').appendTo(header),
          //     header_mobile_menu = header_mobile.find('.header__mobile_menu');

          // header_mobile_menu.append(menu);

        }
      const height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
      var headtop = 0;
      headtop = $('header').innerHeight();
      var blHeight = height - headtop - 9;
      $('.header__mobile').css('height', blHeight);
    },

    menu_toggle: function () {

      var toggleMenu = $('.header__toggle'),
          menuItem = $('.menu__item'),
          menuLink = $('.menu__link');

      toggleMenu.on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            menuMobile = $('.header__mobile'),
            headerMobileTitle = $('.header__mobile_title'),
            menuSubmenu = $('.menu__submenu');

        if (menuMobile.hasClass('-visible')) {

          $('.scroll-up-button').show();

          if (menuMobile.hasClass('-visible-submenu')) {

            $('.scroll-up-button').hide();

            $this.removeClass('-submenu');
            menuMobile.removeClass('-visible-submenu');
            menuSubmenu.removeClass('-show');

            headerMobileTitle.css('opacity', 0);

            setTimeout(function () {

              headerMobileTitle.text('Меню').css('opacity', '');
            }, 500);
          } else {
            menuMobile.removeClass('-visible');
            $this.removeClass('-active');
            if ($('.page-catalog').length) {
              $('.page-catalog').removeClass('menuMobileOpen');
            }
            $('body').css('overflow', '');
          }
        } else {
          $('.scroll-up-button').hide();
          menuMobile.addClass('-visible');
          $this.addClass('-active');
          if ($('.page-catalog').length) {
            $('.page-catalog').addClass('menuMobileOpen');
          }
          $('body').css('overflow', 'hidden');
        }
      });

      menuItem.on('mouseenter', function () {

        var window_width = $(window).width(),
            $this = $(this),
            submenu = $this.find('.menu__submenu');

        if (window_width >= 1200) {
          if (submenu.length) {
            submenu.removeClass('-show');
            $this.addClass('-show-submenu');
            $('.header').addClass('-show-submenu');
            app.overflow.show();
          }
        }
      }).on('mouseleave', function () {

        var window_width = $(window).width(),
            $this = $(this),
            submenu = $this.find('.menu__submenu');

        if (window_width >= 1200) {
          $this.removeClass('-show-submenu');
          submenu.removeClass('-show');
          $('.header').removeClass('-show-submenu');
          app.overflow.hide();
          app.minicart.close();
        }
      });

      menuLink.on('click', function (e) {

        var $this = $(this),
            $thisText = $this.text(),
            $thisItem = $this.closest('.menu__item'),
            $thisSubmenu = $thisItem.find('.menu__submenu'),
            menuMobile = $('.header__mobile'),
            headerMobileTitle = $('.header__mobile_title');

        if ($thisSubmenu.length && $winW < 768) {
          e.preventDefault();

          $thisSubmenu.addClass('-show');
          toggleMenu.addClass('-submenu');
          menuMobile.addClass('-visible-submenu');
          headerMobileTitle.css('opacity', 0);

          setTimeout(function () {
            headerMobileTitle.text($thisText).css('opacity', '');
          }, 500);
        }
      });
    },

    title_cliptext: function () {

      if (isSafari) {

        $('.title-cliptext').each(function () {

          $(this).css('background-image', '').removeClass('title-cliptext');
        });
      }
    }

  },

  scrollbar: {

    init: function () {

      app.scrollbar.scrollPage();
      app.scrollbar.scrollMinicart();
      app.scrollbar.scrollCheckout();
      app.scrollbar.scrollProducts();
    },

    scrollPage: function () {

      var Scrollbar = app.config.Scrollbar;

      scrollbar = Scrollbar.init(document.querySelector('#pageScrollBar'), {
        speed: .6,
        damping: .1,
        continuousScrolling: !1
      });

      scrollbar.addListener(function (a) {

        var scrolled = a.offset.y;
        $scrolled = scrolled;

        app.header.fixed(scrolled);
        app.up_button.show(scrolled);
        app.header.background(scrolled);

        if ($('.menu__item').hasClass('-show-submenu')) {
          app.header.hide_submenu();
        }

        if ($(app.config.minicart).hasClass('-show-minicart')) {
          app.minicart.close();
        }

        //app.checkout.fixed(scrolled);

        if ($('.checkout').hasClass('-show-checkout')) {
          app.checkout.close();
        }

        app.scrollbar.scrollAnimation();
        app.card.hover_close();

        app.parallax.scroll(scrolled);
        app.parallax.paralax_img(scrolled);

        app.scrollbar.scroll_back_fixed(scrolled);
      });

      return scrollbar;
    },

    scrollMinicart: function () {

      if (!$('.minicart__content').length) return;

      var Scrollbar = app.config.Scrollbar;

      scrollbarMinicart = Scrollbar.init($('.minicart__content')[0], {
        speed: .6,
        damping: .1,
        continuousScrolling: !1
      });

      $(app.config.minicart)[0].addEventListener('wheel', e => e.preventDefault()); // останавливаем скролл страницы
    },

    scrollCheckout: function () {

      if (!$('.checkout__wrap').length) return;

      var Scrollbar = app.config.Scrollbar;

      scrollbarCheckout = Scrollbar.init($('.checkout__wrap')[0], {
        speed: .6,
        damping: .1,
        continuousScrolling: !1
      });
    },

    scrollProducts: function () {

      if (!$('.catalog__product_scroll').length) return;

      $('.catalog__product_scroll').each(function () {

        var $this = $(this);

        var Scrollbar = app.config.Scrollbar;

        var scrollbarProduct = Scrollbar.init($this[0], {
          speed: .6,
          damping: .1,
          continuousScrolling: !1
        });
      });
    },

    sale_scroll: function () {

      if ($winW <= 768) {

        if (!$('.sale__status_scroll').length) return;

        var Scrollbar = app.config.Scrollbar;

        scrollbarSale = Scrollbar.init($('.sale__status_scroll')[0], {
          speed: .6,
          damping: .1,
          continuousScrolling: !1
        });
      }
    },

    scrollAnimation: function () {

      $('.catalog__item').length >= 1 && $('.catalog__item').each(function () {
        $(this).offset().top < $winH - 100 && $(this).addClass('-visible');
      });

      $('.animation_up').length >= 1 && $('.animation_up').each(function () {
        $(this).offset().top < $winH - 100 && $(this).addClass('-visible');
      });
    },

    scroll_back_fixed: function (scrolled) {

      if (!$('.cardblog__back').length) return;

      if ($winW >= 768) {

        if (back_offsetTop - 85 < scrolled) {
          $('.cardblog__back').css('top', scrolled + 85 + 'px');
        } else {
          $('.cardblog__back').css('top', '');
        }
      } else {

        if (scrolled != currentScroll) {

          if (scrolled < currentScroll) {
            $('.cardblog__back').addClass('-visible').css('top', scrolled + $winH - 80 + 'px');
          } else {
            $('.cardblog__back').removeClass('-visible').css('top', '');
          }
        }

        currentScroll = scrolled;
      }
    }

  },

  mainscreen: {

    init: function () {

      if (!isSafari) {

        // app.mainscreen.creat();

      }
    },

    creat: function () {

      var mainscreen = $('.mainscreen'),
          mainscreen_wrapper = $('.mainscreen__wrapper'),
          blur,
          img_w,
          img_elem,
          svg_elem,
          img = new Image(),
          img_src;

      if (!mainscreen.length) return;

      mainscreen.prepend('<div class="mainscreen__blur"></div>');

      blur = mainscreen.find('.mainscreen__blur');
      img_src = mainscreen.css('background-image').replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');
      img.src = img_src;
      img = $(img);

      img.on('load', function () {

        blur.append(img);
        img_elem = blur.find('img');
        img_w = img_elem.width();

        moveBlur(img_w);
      });

      function moveBlur(img_w) {

        mainscreen_wrapper.on('mousemove', function (e) {

          blur.addClass('-visible');

          var window_width = $(window).width(),
              img_w = img.width(),
              delta = (window_width - img_w) / 2,
              part = window_width / 3,
              x = e.pageX;

          if (x < part) {
            blur.css({ 'left': 0 });
            img_elem.css({ 'margin-left': delta + 'px' });
          } else if (x > part && x < 2 * part) {
            blur.css({ 'left': part + 'px' });
            img_elem.css({ 'margin-left': delta - part + 'px' });
          } else if (x > 2 * part) {
            blur.css({ 'left': 2 * part + 'px' });
            img_elem.css({ 'margin-left': delta - 2 * part + 'px' });
          }
        }).on('mouseleave', function () {
          blur.removeClass('-visible');
        });
      }
    }

  },

  parallax: {

    init: function () {

      app.parallax.set_stage();
    },

    scroll: function (scrolled) {

      if (!isSafari) {
        $('.mainscreen').length && $('.mainscreen__blur img').css('top', scrolled / 5 + 'px');
      }

      $('.mainscreen').length && $('.mainscreen').css('background-position', 'top ' + scrolled / 5 + 'px' + ' center');

      $('.blog__screen').length && $('.blog__screen').css('background-position', 'top ' + scrolled / 5 + 'px' + ' center');

      $('.bg_parallax').length && $('.bg_parallax').css('background-position', 'top ' + scrolled / 5 + 'px' + ' center');

      $('.pagecountry__bg_item.-active').length && $('.pagecountry__bg_item.-active').css('background-position', 'top ' + scrolled / 5 + 'px' + ' center');
    },

    set_stage: function () {

      $('.parallax').each(function () {

        $(this).attr('style', '');
        var $offset = $(this).offset().top;
        $(this).attr('data-offset', $scrolled + $offset);
        $(this).css('transform', 'translateY(' + -.08 * ($scrolled - $offset) + 'px)');
      });
    },

    paralax_img: function (scrolled) {

      $('.parallax').each(function () {

        var $offset = $(this).attr('data-offset');

        $(this).css('transform', 'translateY(' + -.08 * (scrolled - $offset) + 'px)');
      });
    }

  },

  slider: {

    init: function () {

      app.slider.cardblog();
      app.slider.country();
      app.slider.show();
      app.slider.team();
    },

    init_resize: function () {

      app.slider.mainscreen();
    },

    mainscreen: function () {

      var mainscreen_header = $('.mainscreen__header'),
          mainscreen_wrapper = $('.mainscreen__wrapper'),
          mainscreen_sections = $('.mainscreen__sections'),
          window_width = $(window).width();

      var mainscreen_header_height = mainscreen_header.height();

      mainscreen_wrapper.css('margin-top', -mainscreen_header_height);

      if (window_width >= 1200) {

        if (mainscreen_sections.hasClass('slick-initialized')) {
          mainscreen_sections.slick('unslick');
        }
      } else {

        if (mainscreen_sections.hasClass('slick-initialized')) return;

        mainscreen_sections.slick({
          arrows: false,
          dots: true
        });
      }
    },

    cardblog: function () {

      $('.cardblog__slider').slick({
        fade: true,
        speed: 500,
        responsive: [{
          breakpoint: 768,
          settings: {
            dots: true,
            arrows: false
          }
        }]
      });
    },

    country: function () {

      $('.pagecountry__nav').on('init', function (event, slick) {

        var bg_box = $('<div />', {
          class: 'pagecountry__bg'
        }).prependTo($('.pagecountry__first_screen'));

        var $slides = slick.$slides;

        $slides.each(function () {

          var $slide = $(this),
              $slide_index = $slide.attr('data-slick-index'),
              $slide_item = $slide.find('.pagecountry__nav_item'),
              bg_url = $slide_item.attr('data-bg-url');

          var bg_element = $('<div />', {
            class: $slide_index == 0 ? 'pagecountry__bg_item -active' : 'pagecountry__bg_item',
            css: {
              'background-image': 'url(' + bg_url + ')'
            },
            'data-slick-index': $slide_index
          }).appendTo(bg_box);

          $('.pagecountry__first_screen').attr('style', '');

          $('.pagecountry__content_item').eq($slide_index).attr('data-slick-index', $slide_index);
        });

        product_slider($('.pagecountry__slider').eq(0));

        // $(function () {
        //
        //   var bg_cap = $('<div />', {
        //     class: 'pagecountry__cap'
        //   }).appendTo('.pagecountry__content');
        //
        //   var bg_cap_container = $('<div />', {
        //     class: 'pagecountry__cap_container container'
        //   }).appendTo(bg_cap);
        //
        //   for (var i = 0; i < 3; i++) {
        //
        //     var bg_cap_item = $('<div />', {
        //       class: 'pagecountry__cap_item'
        //     }).appendTo(bg_cap_container);
        //
        //   }
        //
        //   var slider = $('.pagecountry__slider.slick-slider'),
        //       slider_height = slider.height();
        //
        //   bg_cap.height(slider_height);
        //
        //   $(window).on('resize', function () {
        //
        //     slider = $('.pagecountry__slider.slick-slider'),
        //         slider_height = slider.height();
        //     bg_cap.height(slider_height);
        //
        //   });
        //
        //   bg_cap.hide();
        //
        //
        // });

      });

      $('.pagecountry__nav').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

        // $('.pagecountry__cap').show();

        $('.pagecountry__bg_item').removeClass('-active');
        $('.pagecountry__bg_item[data-slick-index="' + nextSlide + '"]').addClass('-active');

        var slide = $('.pagecountry__content_item[data-slick-index="' + nextSlide + '"]');

        $('.pagecountry__content_item').removeClass('-fade');

        setTimeout(function () {
          $('.pagecountry__content_item').removeClass('-active');

          // setTimeout(function () {
          //   $('.pagecountry__cap').hide();
          // }, 700);

          $('.pagecountry__slider').each(function () {

            if ($(this).hasClass('slick-initialized')) {

              $(this).slick('unslick');
            }
          });

          product_slider(slide.find('.pagecountry__slider'));

          slide.addClass('-active').addClass('-fade');
        }, 700);
      });

      $('.pagecountry__nav').slick({
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: 0,
        focusOnSelect: true,
        speed: 1400
      });

      function product_slider(slider) {

        slider.slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          responsive: [{
            breakpoint: 992,
            settings: {
              slidesToShow: 2
            }
          }, {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              arrows: false,
              dots: true
            }
          }]
        });
      }
    },

    show: function () {

      var about_for = $('.about__show_for');
      var about_nav = $('.about__show_nav');

      about_for.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: about_nav,
        responsive: [{
          breakpoint: 992,
          settings: {
            fade: false,
            arrows: false
          }
        }]
      });

      about_nav.on('init', function (event, slick) {
        var slider = $(event.target);
        slider.prepend('<div class="slick-count">' + (slick.currentSlide + 1) + ' / ' + slick.slideCount + '</div>');
      });

      about_nav.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        var slider = $(event.target);
        var slider_count = slider.find('.slick-count');
        slider_count.text(slick.currentSlide + 1 + ' / ' + slick.slideCount);
      });

      about_nav.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: about_for,
        focusOnSelect: true,
        arrows: false,
        responsive: [{
          breakpoint: 992,
          settings: {
            dots: true
          }
        }]
      });
    },

    team: function () {

      var about_for = $('.about__team_for');
      var about_nav = $('.about__team_nav');

      about_for.on('init', function (event, slick) {
        var elements = $('.about__team_for_elements');
        elements.prepend('<div class="slick-count">' + (slick.currentSlide + 1) + ' / ' + slick.slideCount + '</div>');
      });

      about_for.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        var elements = $('.about__team_for_elements');
        var slider_count = elements.find('.slick-count');
        slider_count.text(slick.currentSlide + 1 + ' / ' + slick.slideCount);
      });

      about_for.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: about_nav,
        appendArrows: $('.about__team_for_arrows'),
        responsive: [{
          breakpoint: 992,
          settings: {
            fade: false,
            arrows: false
          }
        }]
      });

      about_nav.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: about_for,
        focusOnSelect: true,
        centerMode: true,
        arrows: false,
        responsive: [{
          breakpoint: 992,
          settings: {
            dots: true
          }
        }]
      });
    }

  },

  store: {

    init_resize: function () {

      var store = $('.store');

      if (!store.length) return;

      var window_width = $(window).width(),
          store_container = store.find('.store__container'),
          store_box = store.find('.store__box'),
          store_grid = store.find('.store__grid'),
          store_mobile = store.find('.store__mobile'),
          store_mobile_slider,
          store_mobile_grid;

      if (window_width > 768) {

        if (store.find('.store__mobile').length) {

          $('.store__product').each(function () {

            var $this_product = $(this);

            store_box.each(function () {

              var $this_box = $(this);

              if ($this_box.hasClass('store__box--product') && !$this_box.find('div').length) {

                $this_box.append($this_product);

                return false;
              }
            });
          });

          $('.store__grid_item').each(function () {

            var $this_grid_item = $(this);

            store_grid.each(function () {

              var $this_grid = $(this);

              if ($this_grid.find('.store__grid_item').length < 4) {

                $this_grid.append($this_grid_item);

                return false;
              }
            });
          });

          store_mobile.remove();
        }
      } else {

        if (store_mobile.length) return;

        store_container.append('<div class="store__mobile">' + '<div class="store__mobile_slider"></div>' + '<div class="store__mobile_grid"></div>' + '</div>');
        store_mobile_slider = store.find('.store__mobile_slider');
        store_mobile_grid = store.find('.store__mobile_grid');

        $('.store__product').appendTo(store_mobile_slider);
        $('.store__grid_item').appendTo(store_mobile_grid);

        $(store_mobile_slider).slick({
          arrows: false,
          dots: true
        });

        var store_mobile_more = $('<a href="#" class="store__mobile_more">Показать еще товар</a>').appendTo(store_mobile_grid);

        store_mobile_more.on('click', function (e) {
          e.preventDefault();

          if (store_mobile_grid.hasClass('store__mobile_grid--more')) {
            store_mobile_grid.removeClass('store__mobile_grid--more');
            store_mobile_more.text('Показать еще товар');
          } else {
            store_mobile_grid.addClass('store__mobile_grid--more');
            store_mobile_more.text('Скрыть товар');
          }
        });
      }
    }

  },

  card: {

    init: function () {

      app.card.toggle();
      app.card.hover_close();
      app.card.cliptext();
    },

    toggle: function () {

      $(document).on('click', '.catalog__product_toggle', function (e) {
        e.preventDefault();

        var $this = $(this),
            product = $this.closest('.catalog__product');

        if (product.hasClass('-hover')) {
          product.removeClass('-hover');
        } else {
          product.addClass('-hover');
        }
      });
    },

    hover_close: function () {

      //$('.catalog__product').length && $('.catalog__product').removeClass('-hover');

    },

    cliptext: function () {

      var cliptext = $('.cardblog').find('.title-cliptext');

      if (!cliptext.length) return;

      var bg_url = $('.cardblog').find('.cardblog__first_screen').css('background-image');

      cliptext.css('background-image', bg_url);
    }

  },

  cardinfo: {

    init: function () {

      $('.cardinfo__quest').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            cardinfo = $this.closest('.cardinfo'),
            cardinfo_item = cardinfo.find('.cardinfo__quest'),
            content = $this.siblings('.cardinfo__content');

        if (content.hasClass('-active')) {

          $this.removeClass('-active');
          content.removeClass('-active');
          content.slideUp();
        } else {

          cardinfo_item.removeClass('-active');
          cardinfo.find('.cardinfo__content').removeClass('-active');
          cardinfo.find('.cardinfo__content').slideUp();

          $this.addClass('-active');
          content.addClass('-active');
          content.slideDown();
        }
      });
    }

  },

  catalog_filter: {

    init: function () {

      app.catalog_filter.catalog();
      app.catalog_filter.open();
      app.catalog_filter.clear();
      app.catalog_filter.link_disabled();
    },

    catalog: function () {

      var filterWrapperHeight = $('.filter_wrapper').innerHeight();
      $('.catalog_filter_wrapper').css('min-height', filterWrapperHeight);

      var winSizeMore992 = false;
      $(window).on('load resize', function () {
        if ($(window).width() > 992) {
          winSizeMore992 = true;
        } else {
          winSizeMore992 = false;
        }
      });

      $(window).on('load', function () {
        $('.header__mobile_menu').mCustomScrollbar({ axis: 'y' });

        if (!winSizeMore992) {
          $('.catalog .filter_wrapper').mCustomScrollbar({
            axis: 'x',
            callbacks: {
              whileScrolling: function () {
                var scroll = $(this).find('.mCSB_container').css('left');
                $(this).find('.mCSB_container').css('margin-left', scroll);
              }
            }
          });

          var filterAccord = $('.catalog_filter'),
              filterAccordBtn = filterAccord.find('.filter__name'),
              filterAccordContent = filterAccord.find('.filter_list');

          filterAccordBtn.on('click', function (e) {
            e.preventDefault();

            var $this = $(this),
                $this_content = $this.closest('.filter_item').find('.filter_list');

            if ($this.hasClass('-active')) {
              $this.removeClass('-active');
              $this_content.fadeOut(300);
            } else {
              filterAccordBtn.removeClass('-active');
              filterAccordContent.fadeOut(300);
              $this.addClass('-active');
              $this_content.fadeIn(300);
            }
          });

          $(document).on('click', function (e) {

            if (!filterAccordBtn.is(e.target) && filterAccordBtn.has(e.target).length === 0 && !filterAccordContent.is(e.target) && filterAccordContent.has(e.target).length === 0) {
              $('.filter__name').removeClass('-active');
              filterAccordContent.fadeOut(300);
            }
          });
        } else {
          $('.catalog .filter_wrapper').mCustomScrollbar('destroy');
        }
      });

      $('.catalog .filter_wrapper .list_wrapper').each(function () {
        var $this = $(this);
        var thischeckbox = $this.find('input[type="checkbox"]');

        thischeckbox.change(function () {
          if ($this.find('input[type="checkbox"]:checked').length > 0) {
            $this.parents('.filter_item').find('.filter__name').addClass('active');
          } else {
            $this.parents('.filter_item').find('.filter__name').removeClass('active');
          }
        });
      });

      var checkbox = $('.catalog_filter input[type="checkbox"]');
      checkbox.change(function () {
        if ($('.catalog_filter input[type="checkbox"]:checked').length > 0) {
          $('.btn_clear').fadeIn(300);
        } else {
          $('.btn_clear').fadeOut(300);
        }
      });

      $('.btn_clear').on('click', function () {
        checkbox.prop('checked', false);
        $(this).fadeOut(300);
        $('.filter__name').removeClass('active');
      });

      $('.clear_inner_filter').on('click', function () {
        $(this).parents('.filter_list').find('input[type="checkbox"]').prop('checked', false);
        $(this).parents('.filter_list').prev('.filter__name').removeClass('active');
      });

      $.fn.toggle2classes = function (class1, class2) {
        if (!class1 || !class2) return this;
        return this.each(function () {
          var $elm = $(this);
          if ($elm.hasClass(class1) || $elm.hasClass(class2)) $elm.toggleClass(class1 + ' ' + class2);else $elm.addClass(class1);
        });
      };
      $('.jsSortPrice').on('click', function (e) {
        e.preventDefault();
        $(this).toggle2classes('up', 'down');
        $('.jsSortTop').removeClass('active');
      });
      $('.jsSortTop').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('active');
        $('.jsSortPrice').removeClass('up down');
      });
    },

    open: function () {

      var filter = $('.catalog__filter'),
          filter_toggle = filter.find('.catalog__filter_link'),
          filter_content = $('.catalog__filter_content');

      filter_toggle.on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('-active');
        filter_content.slideToggle();
      });
    },

    clear: function () {

      var filter_clear = $('.filter__clear');

      filter_clear.on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            checkbox = $this.closest('.filter').find('.checkbox__input');

        checkbox.prop('checked', '');
      });
    },

    link_disabled: function () {

      $('.catalog__product_img').on('click', function (e) {

        if ($winW > 768) {
          e.preventDefault();
        }
      });
    }

  },

  sorting_articles: {

    init: function () {

      app.sorting_articles.scroll();
      app.sorting_articles.select();
    },

    scroll: function () {

      var scroll_btn = $('.goto-sorting'),
          scrollbar = app.scrollbar.scrollPage(),
          articles = $('.articles'),
          articles_btn = articles.find('.articles__sorting_btn'),
          articles_item = articles.find('.articles__sorting_item');

      scroll_btn.on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            id = $this.attr('href'),
            sorting_offset = articles.position().top - 80,
            text = $(id).addClass('-active').text();

        articles_item.removeClass('-active');
        articles_btn.text(text);

        scrollbar.scrollTo(0, sorting_offset, 1000);
      });
    },

    select: function () {

      var sorting = $('.articles__sorting_dropdown'),
          sorting_btn = sorting.find('.articles__sorting_btn'),
          sorting_item = sorting.find('.articles__sorting_item');

      sorting_btn.on('click', function (e) {
        e.preventDefault();

        if (!sorting.hasClass('-show-menu')) {
          sorting.addClass('-show-menu');
        } else {
          sorting.removeClass('-show-menu');
        }

        $(document).on('click', function (e) {

          if (!$(e.target).closest('.articles__sorting_dropdown').length) {
            app.sorting_articles.close();
          }
        });
      });

      sorting_item.on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $this_text = $this.text();

        sorting_btn.text($this_text);
        sorting.removeClass('-show-menu');
        sorting_item.removeClass('-active');
        $this.addClass('-active');
      });
    },

    close: function () {

      $('.articles__sorting_dropdown').length >= 1 && $('.articles__sorting_dropdown').removeClass('-show-menu');
    }

  },

  sorting: {

    init: function () {

      app.sorting.select();
    },

    select: function () {

      var sorting = $('.sorting__dropdown'),
          sorting_btn = sorting.find('.sorting__btn'),
          sorting_item = sorting.find('.sorting__item');

      sorting_btn.on('click', function (e) {
        e.preventDefault();

        if (!sorting.hasClass('-show-menu')) {
          sorting.addClass('-show-menu');
        } else {
          sorting.removeClass('-show-menu');
        }

        $(document).on('click', function (e) {

          if (!$(e.target).closest('.sorting__dropdown').length) {
            app.sorting.close();
          }
        });
      });

      sorting_item.on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $this_text = $this.text();

        sorting_btn.text($this_text);
        sorting.removeClass('-show-menu');
        sorting_item.removeClass('-active');
        $this.addClass('-active');
      });
    },

    close: function () {

      $('.sorting__dropdown').length >= 1 && $('.sorting__dropdown').removeClass('-show-menu');
    }

  },

  profile: {

    init: function () {

      app.profile.history();
      app.profile.add_address();
    },

    history: function () {

      $('.history__count').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $this_item = $this.closest('.history__item'),
            $this_inside = $this_item.find('.history__inside');

        var inside = $('.history__inside');

        if ($this.hasClass('-active')) {

          $this.removeClass('-active');
          $this_inside.slideUp();
        } else {
          inside.slideUp();
          $this.addClass('-active');
          $this_inside.slideDown();
        }
      });
    },

    add_address: function () {

      $('.profile__add-address').on('click', function (e) {
        e.preventDefault();

        var $this = $(this);
        var address_container = $this.siblings('.profile__address');
        var address_item = address_container.find('.profile__address_item');
        var address_item_count = address_item.length + 1;
        var new_item = '<div class="profile__address_item">' + '<input type="text" id="profile_address_' + address_item_count + '" name="profile_address_' + address_item_count + ' ">' + '<a href="#" class="profile__address_checkbox"></a>' + '<a href="#" class="profile__address_remove"></a>' + '</div>';

        address_container.append(new_item);
      });

      $(document).on('click', '.profile__address_remove', function (e) {
        e.preventDefault();

        var $this = $(this),
            $this_item = $this.closest('.profile__address_item');

        $this_item.remove();
      });

      $(document).on('click', '.profile__address_checkbox', function (e) {
        e.preventDefault();

        var $this = $(this),
            $this_item = $this.closest('.profile__address_item');

        $('.profile__address_item').removeClass('-active');
        $this_item.addClass('-active');

        console.log('click');
      });
    }

  },

  pagination: {

    init: function f() {

      app.pagination.history();
    },

    history: function () {

      var history = $('.history'),
          history_list = history.find('.history__list'),
          history_item = history_list.find('.history__item'),
          history_footer = history.find('.history__footer');

      if (history_item.length > 5) {

        var count_pagination = Math.ceil(history_item.length / 5);

        console.log(count_pagination);

        app.pagination.creat_pagination(history_footer, count_pagination);
      }
    },

    creat_pagination: function (wrapper, countLink) {

      var container = wrapper;

      var pagination = $('<div />', {
        class: 'pagination'
      }).appendTo(container);

      var $list = $('<div />', {
        class: 'pagination__list'
      }).appendTo(pagination);

      if (countLink > 6) {
        creat_pagination_dots();
      } else {
        creat_pagination();
      }

      var $prev = $('<a />', {
        href: '#',
        text: 'Пред. страница',
        class: 'pagination__prev -disabled'
      }).prependTo(pagination);

      var $next = $('<a />', {
        href: '#',
        text: 'След. страница',
        class: 'pagination__next'
      }).appendTo(pagination);

      function creat_pagination_dots() {

        for (var i = 1; i <= countLink; i++) {

          if (i > 3 && i < countLink - 2) {

            if (!pagination.find('.pagination__dots').length) {

              var $link = $('<span />', {
                text: '...',
                class: 'pagination__dots'
              }).appendTo($list);
            }
          } else {

            var $link = $('<a />', {
              href: '#',
              text: i
            }).appendTo($list);

            i == 1 && $link.addClass('-active');
          }
        }
      }

      function creat_pagination() {

        for (var i = 1; i <= countLink; i++) {

          var $link = $('<a />', {
            href: '#',
            text: i
          }).appendTo($list);

          i == 1 && $link.addClass('-active');
        }
      }
    }

  },

  video: {

    init: function () {

      app.video.about();
    },

    about: function () {

      $('.about__video_pattern').on('click', function (e) {
        e.preventDefault();

        var src = $(this).attr('href');

        $.magnificPopup.open({

          mainClass: 'mfp-fade',
          removalDelay: 300,
          items: [{
            src: src,
            type: 'iframe'
          }],
          iframe: {
            patterns: {
              vimeo: {
                index: 'vimeo.com/',
                id: '/',
                src: '//player.vimeo.com/video/%id%?autoplay=1&api=1&player_id=playall'
              }
            }
          }
        });
      });
    }

  },

  faq: {

    init: function () {

      app.faq.question();
    },

    question: function () {

      $('.faq__item_question').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $this_content = $this.text(),
            $this_answer = $this.siblings('.faq__item_answer'),
            $this_answer_content = $this_answer.html();

        var answer = $('.faq__answer'),
            answer_question = answer.find('.faq__answer_left'),
            answer_content = answer.find('.faq__answer_right');

        if ($winW > 768) {

          answer_question.hide();
          answer_content.hide();
          answer_question.text($this_content);
          answer_content.html($this_answer_content);
          answer_question.fadeIn();
          answer_content.fadeIn();

          var answer_top = answer.offset().top,
              scroll_top = scrollbar.offset.y,
              offset_top = answer_top + scroll_top - 70;

          scrollbar.scrollTo(0, offset_top, 600);
        } else {

          if ($this.hasClass('-active')) {

            $this.removeClass('-active');
            $this_answer.slideUp();
          } else {

            $('.faq__item_question').removeClass('-active');
            $('.faq__item_answer').slideUp();
            $this.addClass('-active');
            $this_answer.slideDown();
          }
        }
      });
    }

  },

  jobs: {

    init: function () {

      app.jobs.show();
      app.jobs.file_upload();
    },

    show: function () {

      $('.jobs__show').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $this_item = $this.closest('.jobs__item'),
            $this_content = $this_item.find('.jobs__content');

        if ($this.hasClass('-active')) {

          $this.removeClass('-active').text('Показать полностью');
          $this_content.slideUp();
        } else {

          $('.jobs__content').slideUp();
          $('.jobs__show').removeClass('-active').text('Показать полностью');

          $this.addClass('-active').text('Скрыть');
          $this_content.slideDown();
        }
      });
    },

    file_upload: function () {

      var upload = $('.file-upload'),
          upload_file = upload.find('.file-upload__file'),
          upload_filename = upload.find('.file-upload__label');

      upload_file.on('change', function () {

        var $this = $(this),
            val = $this.val().replace(/.*\\/, '');

        if (val.length) {
          upload_filename.text(val);
        } else {
          upload_filename.text('Прикрепить резюме');
        }
      });
    }

  },

  diagram: function () {

    var diagram = $('.diagram'),
        count_dashed;

    if (!diagram.length) return;

    diagram.each(function () {

      var svg = $(this).find('svg'),
          group_dashed = svg.find('.diagram__line'),
          count_dashed = +svg.attr('data-count') + 1,
          diagram_coord = [],
          diagram_group,
          value = svg.attr('data-value').split(',');

      $(SVG('g')).attr('class', 'diagram-group').appendTo(svg);

      diagram_group = svg.find('.diagram-group');

      group_dashed.each(function (index) {
        var $this = $(this),
            path = $this.find('path'),
            coord = getCoord(path);

        addDashed($this, coord);

        addValue(coord, value[index]);

        diagram_coord = $.merge(diagram_coord, addValue(coord, value[index]));

        drawCircle(diagram_group, addValue(coord, value[index]));
      });

      drawDiagram(diagram_group, diagram_coord);
      drawDiagramFill(diagram_group, diagram_coord);

      function SVG(tag) {
        return document.createElementNS('http://www.w3.org/2000/svg', tag);
      }

      function getCoord(elem) {

        var d = elem.attr('d');
        var coord = d.replace(/[A-Z-a-z]/g, '').split(' ');

        return coord;
      }

      function addValue(coord, value) {

        var coord = coord,
            x1 = +coord[0],
            y1 = +coord[1],
            x2 = +coord[2],
            y2 = +coord[3],
            step_x = x1 < x2 ? (x2 - x1) / count_dashed : (x1 - x2) / count_dashed,
            step_y = y1 < y2 ? (y2 - y1) / count_dashed : (y1 - y2) / count_dashed;

        var x = step_x * value,
            y = step_y * value;

        if (x2 > 200 && y2 < 200) {
          x = x1 + x;
          y = y1 - y;
        } else if (x2 > 200 && y2 > 200) {
          x = x1 + x;
          y = y1 + y;
        } else if (x2 < 200 && y2 > 200) {
          x = x1 - x;
          y = y1 + y;
        } else if (x2 < 200 && y2 < 200) {
          x = x1 - x;
          y = y1 - y;
        } else {
          x = x1 - x;
          y = y1 - y;
        }

        return [x, y];
      }

      function addDashed(group, coord) {

        var coord = coord,
            x1 = +coord[0],
            y1 = +coord[1],
            x2 = +coord[2],
            y2 = +coord[3],
            step_x = x1 < x2 ? (x2 - x1) / count_dashed : (x1 - x2) / count_dashed,
            step_y = y1 < y2 ? (y2 - y1) / count_dashed : (y1 - y2) / count_dashed,
            newstep_x = 0,
            newstep_y = 0;

        for (var i = 0; i < count_dashed - 1; i++) {

          var newCoordDashed = [];

          newstep_x = newstep_x + step_x;
          newstep_y = newstep_y + step_y;

          var x2_plus = x2 + newstep_x,
              y2_plus = y2 + newstep_y,
              x2_minus = x2 - newstep_x,
              y2_minus = y2 - newstep_y;

          if (x2 > 200 && y2 < 200) {

            newCoordDashed[0] = x2_minus - 1;
            newCoordDashed[1] = y2_plus - 4;
            newCoordDashed[2] = x2_minus + 1;
            newCoordDashed[3] = y2_plus + 4;
          } else if (x2 > 200 && y2 > 200) {

            newCoordDashed[0] = x2_minus + 4;
            newCoordDashed[1] = y2_minus - 2;
            newCoordDashed[2] = x2_minus - 4;
            newCoordDashed[3] = y2_minus + 3;
          } else if (x2 < 200 && y2 > 200) {

            newCoordDashed[0] = x2_plus - 4;
            newCoordDashed[1] = y2_minus - 2;
            newCoordDashed[2] = x2_plus + 4;
            newCoordDashed[3] = y2_minus + 3;
          } else if (x2 < 200 && y2 < 200) {

            newCoordDashed[0] = x2_plus + 1;
            newCoordDashed[1] = y2_plus - 4;
            newCoordDashed[2] = x2_plus - 1;
            newCoordDashed[3] = y2_plus + 4;
          } else {

            newCoordDashed[0] = x2_plus - 4;
            newCoordDashed[1] = y2_plus;
            newCoordDashed[2] = x2_plus + 4;
            newCoordDashed[3] = y2_plus;
          }

          drawPath(group, newCoordDashed);
        }
      }

      function drawPath(elem, coord) {

        var d = 'M';

        for (var i = 0; i < coord.length; i++) {
          d = d + coord[i] + ' ';
        }

        $(SVG('path')).attr('d', d).attr('fill', 'transparent').attr('stroke', '#c5cbcf').attr('stroke-width', '1').appendTo(elem);
      }

      function drawCircle(elem, coord) {

        var cy = coord[0];
        var cx = coord[1];

        $(SVG('circle')).attr('cx', cy).attr('cy', cx).attr('r', 5).attr('fill', '#307fec').appendTo(elem);
      }

      function drawDiagram(elem, coord) {

        var d = 'M';

        for (var i = 0; i < coord.length; i++) {
          d = d + coord[i] + ' ';
        }

        d = d + 'z';

        $(SVG('path')).attr('d', d).attr('fill', 'transparent').attr('stroke', '#307fec').attr('stroke-width', '2').appendTo(elem);
      }

      function drawDiagramFill(elem, coord) {

        var d = 'M';

        for (var i = 0; i < coord.length; i++) {
          d = d + coord[i] + ' ';
        }

        d = d + 'z';

        $(SVG('path')).attr('d', d).attr('fill', '#307fec').attr('opacity', '0.3').appendTo(elem);
      }
    });
  },

  magnificPopup: function () {

    $('.openPopup').magnificPopup({
      mainClass: 'mfp-fade',
      removalDelay: 300
    });
  },

  sideBarMobile: function () {

    $('.js-side__btn').on('click', function () {

      var $this = $(this),
          container = $this.closest('.js-side'),
          content = container.find('.js-side__content');

      if ($(window).width() < 768) {
        $this.toggleClass('-active');
        content.slideToggle();
      }
    });

    $(window).on('resize', function () {

      if (Modernizr.touch && $(window).width() < 1024) {
        $('.js-side').addClass('-touch');
      } else {
        $('.js-side').removeClass('-touch');
      }
    }).trigger('resize');
  },

  tabs: function () {

    $('.b-tabs').each(function (index, element) {

      var tabs = $(element),
          tab = tabs.find('.b-tabs__tab'),
          content = tabs.find('.b-tabs__item');

      tab.each(function (index, element) {
        $(this).attr('data-tab', index);
      });

      showContent(0);

      function showContent(i) {
        tab.removeClass('-active');
        tab.eq(i).addClass('-active');
        content.removeClass('-fade');
        content.removeClass('-active');
        content.eq(i).addClass('-active');
        setTimeout(function () {
          content.eq(i).addClass('-fade');
        }, 10);
      }

      tab.on('click', function () {
        showContent(parseInt($(this).attr('data-tab')));
      });
    });
  },

  accord: function () {

    $('.b-accord__title').on('click', function (e) {
      e.preventDefault();

      var $this = $(this),
          item = $this.closest('.b-accord__item'),
          content = item.find('.b-accord__content');

      if ($this.hasClass('-active')) {
        $this.removeClass('-active');
        content.slideUp();
      } else {
        $('.b-accord__title').removeClass('-active');
        $('.b-accord__content').slideUp();
        $this.addClass('-active');
        content.slideDown();
      }
    });
  },

  form: function () {

    // Show password
    $(function () {

      $('.js-pass').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            input = $this.siblings('input');

        if (input.prop('type') === 'text') {
          input.prop('type', 'password');
          $this.prop('title', 'Show password');
          $this.removeClass('-hidden');
        } else {
          input.prop('type', 'text');
          $this.prop('title', 'Hide password');
          $this.addClass('-hidden');
        }
      });
    });
  },

  data_mask: function () {

    $.mask.definitions['m'] = '[0-1]';
    $.mask.definitions['d'] = '[0-3]';
    $('input[name="shipping_date"]').mask('d9.m9.99', { placeholder: 'дд.мм.гг' });
    $('input[name="profile_date_of_birth"]').mask('d9.m9.99', { placeholder: 'дд.мм.гг' });
    $('input[type="tel"]').mask('+7 (999) 999-99-99');
  },

  validate: function () {

    $('#checkout').validate({
      errorPlacement: function (error, element) {},
      submitHandler: function (form) {
        $('.checkout__error').removeClass('-visible');
      },
      invalidHandler: function (event, validator) {
        $('.checkout__error').addClass('-visible');
      }
    });

    $('#mainform').validate({
      errorPlacement: function errorPlacement(error, element) {},
      submitHandler: function (form) {
        $.ajax({
          type: $(form).attr('method'),
          url: $(form).attr('action'),
          data: $(form).serialize(),
          success: function (e) {
            // console.log(e);
            alert('Данные отправлены');
          },
          error: function (e) {
            // console.log(e);
            alert('Ошибка!');
          }
        });
      }
    });

    $('#contactform').validate({
      errorPlacement: function errorPlacement(error, element) {},
      submitHandler: function (form) {
        $.ajax({
          type: $(form).attr('method'),
          url: $(form).attr('action'),
          data: $(form).serialize(),
          success: function (e) {
            // console.log(e);
            alert('Данные отправлены');
          },
          error: function (e) {
            // console.log(e);
            alert('Ошибка!');
          }
        });
      }
    });

    $('#signin').validate({
      errorPlacement: function errorPlacement(error, element) {},
      submitHandler: function submitHandler(form) {

        $.magnificPopup.open({
          items: {
            src: '#complete_profile',
            type: 'inline'
          },
          removalDelay: 300,
          mainClass: 'mfp-fade'
        });
      }
    });
  },

  filter: function () {

    var filter_name = $('.b-filter__name'),
        filter_reset = $('.b-filter__reset'),
        filter_checkbox = $('.b-filter__checkbox input'),
        filter_row = $('.b-filter__row'),
        filter_item = $('.b-filter__item');

    filter_name.on('click', function (e) {
      e.preventDefault();

      var $this = $(this),
          row = $this.closest('.b-filter__row'),
          hidden = row.find('.b-filter__hidden'),
          list = row.find('.b-filter__list'),
          checkbox_checked = row.find('.b-filter__checkbox input:checked').closest('.b-filter__item');

      if (row.hasClass('-open')) {
        row.removeClass('-open');
        $this.removeClass('-active');
        hidden.slideUp();
      } else {
        row.addClass('-open');
        $this.addClass('-active');
        checkbox_checked.prependTo(list);
        hidden.slideDown();
      }
    });

    filter_reset.on('click', function () {
      filter_checkbox.prop('checked', '');
      $('.b-filter__name--count').remove();
      $(this).removeClass('-active');
    });

    filter_checkbox.on('change', function () {

      var chekced_all = $('.b-filter__checkbox input:checked');

      if (chekced_all.length > 0) {
        filter_reset.addClass('-active');
      } else {
        filter_reset.removeClass('-active');
      }

      var $this = $(this),
          row = $this.closest('.b-filter__row'),
          name = row.find('.b-filter__name'),
          checked = row.find('.b-filter__checkbox input:checked').length,
          count = row.find('.b-filter__name--count');

      if (checked > 0) {

        if (count.length) {
          count.text(' (' + checked + ')');
        } else {
          name.append('<span class="b-filter__name--count"> (' + checked + ')</span>');
        }
      } else {
        count.remove();
      }
    });

    filter_row.each(function () {

      var $this = $(this),
          hidden = $this.find('.b-filter__hidden'),
          list = $this.find('.b-filter__list'),
          item = $this.find('.b-filter__item'),
          limit_item = $this.find('.b-filter__list').data('limit-item'),
          checkbox_item = $this.find('.b-filter__item');

      item.each(function (index, elem) {

        if (index > limit_item - 1) {
          $(elem).hide();
        }
      });

      var checkbox_count = checkbox_item.length,
          checkbox_hidden = checkbox_count - limit_item;

      if (checkbox_count > limit_item) {
        hidden.append('' + '<div class="b-filter__more">' + '<a href="#" class="b-filter__more--link">See more (' + checkbox_hidden + ')</a>' + '</div>');
      }
    });

    filter_row.on('click', '.b-filter__more--link', function (e) {
      e.preventDefault();

      var $this = $(this),
          more = $this.closest('.b-filter__more'),
          row = $this.closest('.b-filter__row'),
          item = row.find('.b-filter__item'),
          scroll = row.find('.b-filter__scroll'),
          search = row.find('.b-filter__search');

      item.show();
      row.addClass('-more');
      more.hide();
      scroll.mCustomScrollbar();

      if (item.length > 10) {
        search.slideDown();
      }
    });
  },

  custom_select: {

    init: function () {
      app.custom_select.creat();
    },

    creat: function () {

      $('select.custom-select').each(function () {
        var $this = $(this),
            numberOfOptions = $(this).children('option').length;

        $this.addClass('custom-select__hidden');
        $this.wrap('<div class="custom-select__select"></div>');
        $this.after('<div class="custom-select__styled"></div>');

        var $styledSelect = $this.next('div.custom-select__styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
          'class': 'custom-select__options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
          $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
          }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function (e) {
          e.stopPropagation();
          $('div.custom-select__styled.active').not(this).each(function () {
            $(this).removeClass('-active').next('ul.custom-select__options').hide();
          });
          $(this).toggleClass('-active').next('ul.custom-select__options').toggle();
        });

        $listItems.click(function (e) {
          e.stopPropagation();
          $styledSelect.text($(this).text()).removeClass('-active');
          $this.val($(this).attr('rel'));
          $list.hide();
          $listItems.removeClass('-active');
          $(this).addClass('-active');
          console.log($this.val());
        });

        $this.on('change', function () {
          console.log($(this).val());
        });

        $(document).click(function () {
          $styledSelect.removeClass('active');
          $list.hide();
        });
      });
    }

  }

};

$(document).ready(app.init());