'use strict';

var app = {

  init: function () {

    app.gotoup();
    app.search();
    app.menu();
    app.nav();
    app.language();
    app.subscribe();
    app.footerList();
    app.dropdown();
    app.scrollBar();
    app.magnificPopup();
    app.slickCarousel();
    app.fileUpload();
    app.sideBarMobile();
    app.tabs();
    app.accord();
    app.form();
    app.tags();
    app.favourites();
    app.validate();
    app.filter();
    app.sorting();
    app.all();
    app.rating();
    app.card();
    app.upload();
    app.see_more();
    app.testim();
    app.comments();
    app.tops();
    app.makeacomp();
    app.gameslist();
    app.newyear();

  },

  gotoup: function () {

    var gotoup = $('.e-gotoup'),
      w = $(window);

    w.on('scroll', function () {

      var scrollTop = w.scrollTop();

      if (scrollTop > 400) {
        gotoup.addClass('-active');
      } else {
        gotoup.removeClass('-active');
      }

    });

    gotoup.on('click', function () {

      $('html, body').animate({scrollTop: 0}, 500);

    });

  },

  menu: function () {

    var toggle = $('.js-menu__toggle'),
      toggle_container = $('.l-header__toggle'),
      menu = $('.b-menu'),
      close = $('.b-menu__close'),
      overlay = $('.b-menu__overlay'),
      item = $('.b-menu__item'),
      link = $('.b-menu__link');

    toggle.on('click', function (e) {
      e.preventDefault();

      if (toggle.hasClass('-active')) {
        toggle.removeClass('-active');
        menu.removeClass('-active');
        $('body').css('overflow', '');
      } else {
        toggle.addClass('-active');
        menu.addClass('-active');
        $('body').css('overflow', 'hidden');
      }

    });

    close.on('click', function (e) {
      e.preventDefault();
      toggle.removeClass('-active');
      menu.removeClass('-active');
      $('body').css('overflow', '');
    });

    overlay.on('click', function (e) {
      toggle.removeClass('-active');
      menu.removeClass('-active');
      $('body').css('overflow', '');
    });

    link.on('click', function (e) {

      if ($(window).width() < 768) {
        e.preventDefault();

        var $this = $(this),
          $this_item = $this.closest('.b-menu__item');

        if ($this_item.hasClass('-active')) {
          $this.removeClass('-active');
          $this_item.removeClass('-active');
        } else {
          link.removeClass('-active');
          item.removeClass('-active');
          $this.addClass('-active');
          $this_item.addClass('-active');
        }
      }

    });

    $(window).on('resize', function () {
      var submenu = $('.b-submenu');
      var list = $('.js-scrollBar.b-submenu__list');

      if ($(window).width() >= 768) {

        if (list.length) {

          var wh = $(window).height();

          list.each(function () {
            var list = $(this),
                top = list.offset().top,
                height = wh - top;

              list.css('max-height', height);
          });

        }

      } else {
        list.css('max-height', '');

        if (!submenu.find('.b-submenu__back').length) {

          submenu.prepend('<button class="b-submenu__back" type="button"></button>');

          $('.b-submenu__back').on('click', function () {
            $(this).closest('.b-menu__item').removeClass('-active');
          });
        }
      }

    }).trigger('resize');

    $(window).on('scroll', function () {

      if ($(window).scrollTop() > 150) {
        toggle_container.addClass('-opacity');

      }
      if ($(window).scrollTop() > 300) {
        toggle_container.addClass('-fixed');
      } else {
        toggle_container.removeClass('-fixed -opacity');
      }

    });

  },

  nav: function nav() {

    var nav = $('.b-nav'),
      nav_container = $('.b-nav__container'),
      nav_scroll = $('.b-nav__scroll'),
      nav_list = $('.b-nav__list'),
      item = nav.find('li');

    if (!nav.length) {
      return;
    }

    nav_list.prepend('<li class="b-nav__line"></li>');

    var line = nav.find('.b-nav__line'),
      timeout;

    $(window).on('resize', function () {

      if (Modernizr.touch || $(window).width() < 992) {
        $('.b-nav__scroll').mCustomScrollbar('destroy');
      } else {
        $('.b-nav__scroll').mCustomScrollbar({
          axis: 'x'
        });
      }

      if (nav_scroll.hasClass('mCS_no_scrollbar') || nav_scroll.hasClass('mCS_destroyed') || $(window).width() < 992) {
        nav.removeClass('-scroll');
      } else {
        nav.addClass('-scroll');
      }

    }).trigger('resize');

    $(window).on('scroll', function () {
      if ($(window).scrollTop() > 200) {
        nav.addClass('-fixed');
      } else {
        nav.removeClass('-fixed');
      }
    });

    $('.b-nav__toggle').on('click', function () {

      var $this = $(this);

      if ($this.hasClass('-active')) {
        $this.removeClass('-active');
        $('.b-nav__scroll').mCustomScrollbar('scrollTo', 'left');
      } else {
        $this.addClass('-active');
        $('.b-nav__scroll').mCustomScrollbar('scrollTo', 'right');
      }

    });

    $('.js-scrolltosection').on('click', 'a', function (e) {
      e.preventDefault();

      var elementScroll = $(this).attr('href').replace(/\//gi, '');

      $('html, body').animate({
        scrollTop: $(elementScroll).offset().top - 20
      }, 800);

    });

  },

  search: function () {

    var input = $('.js-search__input'),
      reset = input.closest('.js-search').find('.js-search__reset');

    input.on('keypress change input', function () {

      var input = $(this),
        reset = input.closest('.js-search').find('.js-search__reset');

      if (input.val().length > 0) {
        reset.addClass('-active');
      } else {
        reset.removeClass('-active');
      }
    });

    reset.on('click', function () {

      var reset = $(this),
        input = reset.closest('.js-search').find('.js-search__reset');

      input.val('');
      reset.removeClass('-active');
    });

  },

  language: function () {

    $('.js-lang__current').on('click', function () {

      var $this = $(this),
        current_img = $this.find('.js-lang__img'),
        container = $this.closest('.js-lang'),
        dropdown = container.find('.js-lang__dropdown'),
        btn = container.find('.js-lang__btn');

      dropdown.toggleClass('-active');

      $(document).on('click', function (e) {

        if (!$(e.target).closest('.js-lang').length) {
          dropdown.removeClass('-active');
        }

      });

    });

    $('.js-lang__btn').on('click', function (e) {
      e.preventDefault();

      var $this = $(this),
        container = $this.closest('.js-lang'),
        current_img = container.find('.js-lang__current .js-lang__img'),
        dropdown = container.find('.js-lang__dropdown'),
        btn = container.find('.js-lang__btn');

      dropdown.removeClass('-active');

      var $this_img = $this.find('.js-lang__img').prop('style'),
        $this_img = $this_img.backgroundImage;

      current_img.css('background-image', $this_img);

    });

  },

  subscribe: function () {

    var subscribe = $('.js-subscribe__btn'),
      popup = $('.js-subscribe__popup');

    subscribe.on('click', function (e) {
      e.preventDefault();

      popup.toggleClass('-active');

      $(document).on('click', function (e) {

        if (!$(e.trigger).closest('.js-subscribe__popup').length) {
          popup.tremoveClass('-active');
        }
      });

    });
  },

  dropdown: function () {

    $('.js-drop__btn').on('click', function (e) {
      e.preventDefault();

      var drop = $(this).closest('.js-drop'),
        dropdown = drop.find('.js-drop__dropdown');

      dropdown.toggleClass('-active');

      $(document).on('click', function (event) {

        if (!$(event.target).closest('.js-drop').length) {
          dropdown.removeClass('-active');
        }

      });

    });


  },

  scrollBar: function () {

    $('.js-scrollBar').mCustomScrollbar();

  },

  footerList: function () {

    $('.js-list__btn').on('click', function (e) {

      if ($(window).width() < 768) {
        e.preventDefault();

        var list = $(this).closest('.js-list'),
          slide = list.find('.js-list__slide');

        if (list.hasClass('-active')) {
          list.removeClass('-active');
          slide.slideUp();
        } else {
          $('.js-list').removeClass('-active');
          $('.js-list__slide').slideUp();
          list.addClass('-active');
          slide.slideDown();
        }

      }


    });

  },

  magnificPopup: function () {

    $('.open-join').magnificPopup({
      mainClass: 'mfp-fade mfp-full',
      removalDelay: 300
    });

    $('.open-modals').magnificPopup({
      mainClass: 'mfp-fade',
      removalDelay: 300
    });

    $('.jsPopupVideo').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 300
    });

    $('.jsGallery').each(function () {
      $(this).magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
          enabled: true
        },
        mainClass: 'mfp-fade',
        removalDelay: 300
      });
    });

  },

  slickCarousel: function () {

    var main_slider = $('.js-main-slider');
    var tops_slider = $('.js-tops-slider');

    $(window).on('resize', function () {

      if ($(window).width() < 768) {
        if (!main_slider.hasClass('slick-initialized')) {
          main_slider.slick({
            arrows: false,
            dots: true,
            adaptiveHeight: true
          });
        }
      } else {
        if (main_slider.hasClass('slick-initialized')) {
          main_slider.slick('unslick');
        }
      }

      if ($(window).width() < 992) {
        if (!tops_slider.hasClass('slick-initialized')) {
          tops_slider.slick({
            arrows: false,
            slidesToShow: 1,
            adaptiveHeight: true,
            dots: true
          });
        }
      } else {
        if (tops_slider.hasClass('slick-initialized')) {
          tops_slider.slick('unslick');
        }
      }

    }).trigger('resize');

  },

  sideBarMobile: function () {

    $('.js-side__btn').on('click', function () {

      var $this = $(this),
        container = $this.closest('.js-side'),
        content = container.find('.js-side__content');

      if ($(window).width() < 768) {
        $this.toggleClass('-active');
        content.slideToggle();
      }
    });

    $(window).on('resize', function () {

      if (Modernizr.touch && $(window).width() < 1024) {
        $('.js-side').addClass('-touch');
      } else {
        $('.js-side').removeClass('-touch');
      }

    }).trigger('resize');

  },

  tabs: function () {

    $('.b-tabs').each(function (index, element) {

      var tabs = $(element),
        tab = tabs.find('.b-tabs__tab'),
        content = tabs.find('.b-tabs__item');

      tab.each(function (index, element) {
        $(this).attr('data-tab', index);
      });

      showContent(0);

      function showContent(i) {
        tab.removeClass('-active');
        tab.eq(i).addClass('-active');
        content.removeClass('-fade');
        content.removeClass('-active');
        content.eq(i).addClass('-active');
        setTimeout(function () {
          content.eq(i).addClass('-fade');
        }, 10);
      }

      tab.on('click', function () {
        showContent(parseInt($(this).attr('data-tab')));
      });

    });

  },

  accord: function () {

    $('.b-accord__title').on('click', function (e) {
      e.preventDefault();

      var $this = $(this),
          item = $this.closest('.b-accord__item'),
          content = item.find('.b-accord__content');

      if ($this.hasClass('-active')) {
        $this.removeClass('-active');
        content.slideUp();
      } else {
        $('.b-accord__title').removeClass('-active');
        $('.b-accord__content').slideUp();
        $this.addClass('-active');
        content.slideDown();
      }


    });

  },

  form: function () {

    // Show password
    $(function () {

      $('.js-pass').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
          input = $this.siblings('input');

        if (input.prop('type') === 'text') {
          input.prop('type', 'password');
          $this.prop('title', 'Show password');
          $this.removeClass('-hidden');
        } else {
          input.prop('type', 'text');
          $this.prop('title', 'Hide password');
          $this.addClass('-hidden');
        }
      });

    });

  },

  tags: function () {

    $('.e-tags').on('click', '.e-tags__list-link', function (e) {
      e.preventDefault();

      var $this = $(this),
        $this_item = $this.closest('.e-tags__list-item'),
        wrap = $this.closest('.e-tags'),
        container = wrap.find('.e-tags__container'),
        tag = $this.text();

      container.append('' +
        '<span class="e-tags__item e-tags__item">' +
        '<span class=" e-tags__text">' + tag + '</span>' +
        '<button class=" e-tags__delete e-tags__delete"></button>' +
        '</span>');

      $this_item.remove();

    });

    $('.e-tags').on('click', '.e-tags__delete', function (e) {
      e.preventDefault();

      var $this = $(this),
        $this_item = $this.closest('.e-tags__item'),
        $this_text = $this_item.find('.e-tags__text'),
        wrap = $this.closest('.e-tags'),
        list = wrap.find('.e-tags__list'),
        tag = $this_text.text();

      list.append('' +
        '<li class="e-tags__list-item e-tags__list-item">' +
        '<a class=" e-tags__list-link" href="#">' + tag + '</a>' +
        '</li>');

      $this_item.remove();

    });

  },

  favourites: function () {

    $('.b-favourites__content').append(
      '<div class="b-favourites__item"></div>' +
      '<div class="b-favourites__item"></div>' +
      '<div class="b-favourites__item"></div>'
    );

    $('.b-favourites__more').on('click', function (e) {

      var $this = $(this),
        container = $this.closest('.b-favourites__section'),
        item = container.find('.b-favourites__item'),
        less = container.find('.b-favourites__hide');

      item.each(function () {
        $(this).addClass('-active');
      });

      if ($this.hasClass('-active')) {
        $this.removeClass('-active');
        $this.text('see more');
        less.fadeOut(500);
        item.removeClass('-fade');
        setTimeout(function () {
          item.removeClass('-active');
        }, 500);
      } else {
        $this.addClass('-active');
        $this.text('see less');
        less.fadeIn(500);
        item.addClass('-active');
        setTimeout(function () {
          item.addClass('-fade');
        }, 1);
      }

    });

    $('.b-favourites__less').on('click', function (e) {

      var $this = $(this),
        hide = $this.closest('.b-favourites__hide'),
        container = $this.closest('.b-favourites__section'),
        item = container.find('.b-favourites__item'),
        more = container.find('.b-favourites__more');

      hide.fadeOut();
      more.removeClass('-active');
      more.text('see more');
      item.removeClass('-fade');
      setTimeout(function () {
        item.removeClass('-active');
      }, 500);

    });

    $(document).on('click', '.b-favourites__delete', function (e) {
      $(this).closest('.b-favourites__item').remove();
    });

    $(document).on('click', '.e-bonuse__info', function (e) {

      var $this = $(this),
        container = $this.closest('.e-bonuse');

      container.addClass('-active');

      $(document).on('click', function (e) {
        if ($(e.target).closest('.e-bonuse')[0] !== container[0]) {
          container.removeClass('-active');
        }
      });

    });

    $(document).on('click', '.e-bonuse__back', function (e) {

      var $this = $(this),
        container = $this.closest('.e-bonuse');

      container.removeClass('-active');

    });

  },

  validate: function () {

    $('#signi').validate({
      errorPlacement: function (error, element) {
      },
      submitHandler: function (form) {


      }
    });


    $('#signin').validate({
      errorPlacement: function errorPlacement(error, element) {
      },
      submitHandler: function submitHandler(form) {

        $.magnificPopup.open({
          items: {
            src: '#complete_profile',
            type: 'inline'
          },
          removalDelay: 300,
          mainClass: 'mfp-fade'
        });

      }
    });

  },

  filter: function () {

    var filter_name = $('.b-filter__name'),
      filter_reset = $('.b-filter__reset'),
      filter_checkbox = $('.b-filter__checkbox input'),
      filter_row = $('.b-filter__row'),
      filter_item = $('.b-filter__item');

    filter_name.on('click', function (e) {
      e.preventDefault();

      var $this = $(this),
        row = $this.closest('.b-filter__row'),
        hidden = row.find('.b-filter__hidden'),
        list = row.find('.b-filter__list'),
        checkbox_checked = row.find('.b-filter__checkbox input:checked').closest('.b-filter__item');

      if (row.hasClass('-open')) {
        row.removeClass('-open');
        $this.removeClass('-active');
        hidden.slideUp();
      } else {
        row.addClass('-open');
        $this.addClass('-active');
        checkbox_checked.prependTo(list);
        hidden.slideDown();
      }

    });

    filter_reset.on('click', function () {
      filter_checkbox.prop('checked', '');
      $('.b-filter__name--count').remove();
      $(this).removeClass('-active');
    });

    filter_checkbox.on('change', function () {

      var chekced_all = $('.b-filter__checkbox input:checked');

      if (chekced_all.length > 0) {
        filter_reset.addClass('-active');
      } else {
        filter_reset.removeClass('-active');
      }

      var $this = $(this),
        row = $this.closest('.b-filter__row'),
        name = row.find('.b-filter__name'),
        checked = row.find('.b-filter__checkbox input:checked').length,
        count = row.find('.b-filter__name--count');

      if (checked > 0) {

        if (count.length) {
          count.text(' (' + checked + ')')
        } else {
          name.append('<span class="b-filter__name--count"> (' + checked + ')</span>');
        }

      } else {
        count.remove();
      }

    });

    filter_row.each(function () {

      var $this = $(this),
        hidden = $this.find('.b-filter__hidden'),
        list = $this.find('.b-filter__list'),
        item = $this.find('.b-filter__item'),
        limit_item = $this.find('.b-filter__list').data('limit-item'),
        checkbox_item = $this.find('.b-filter__item');

      item.each(function (index, elem) {

        if (index > limit_item - 1) {
          $(elem).hide();
        }

      });

      var checkbox_count = checkbox_item.length,
        checkbox_hidden = checkbox_count - limit_item;

      if (checkbox_count > limit_item) {
        hidden.append('' +
          '<div class="b-filter__more">' +
          '<a href="#" class="b-filter__more--link">See more (' + checkbox_hidden + ')</a>' +
          '</div>'
        );
      }

    });

    filter_row.on('click', '.b-filter__more--link', function (e) {
      e.preventDefault();

      var $this = $(this),
        more = $this.closest('.b-filter__more'),
        row = $this.closest('.b-filter__row'),
        item = row.find('.b-filter__item'),
        scroll = row.find('.b-filter__scroll'),
        search = row.find('.b-filter__search');

      item.show();
      row.addClass('-more');
      more.hide();
      scroll.mCustomScrollbar();

      if (item.length > 10) {
        search.slideDown();
      }

    });

  },

  sorting: function () {

    $('.b-sorting__item').on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('-active');
    });

  },

  all: function () {

    $('.b-all__content').append(
      '<div class="b-all__item"></div>' +
      '<div class="b-all__item"></div>'
    );

  },

  rating: function () {

    $('.e-rating.-read').each(function () {

      var $this = $(this),
        select = $this.find('select'),
        currentRating = select.data('current-rating'),
        value = $this.find('.e-rating__value');

      select.barrating({
        theme: 'fontawesome-stars-o',
        readonly: true,
        initialRating: currentRating
      });

      value.text(currentRating + '/10');


    });

    $('.e-rating.-select').each(function () {

      var $this = $(this),
        select = $this.find('select');

      select.barrating({
        theme: 'fontawesome-stars-o'
      });

    });


  },

  card: function () {

    $('.b-topmenu__link').on('click', function (e) {
      e.preventDefault();

      var $this = $(this),
        $this_item = $this.closest('.b-topmenu__item');

      $('.b-topmenu__item').removeClass('-active');
      $this_item.addClass('-active');

    });

    $('.b-topmenu__addtofaw').on('click', function (e) {
      e.preventDefault();

      $(this).toggleClass('-active');

    });

    $(window).on('load', function () {

      var topmenu = $('.b-topmenu');

      if (topmenu.length) {

        var topmenu_top = topmenu.offset().top;

        $(window).on('scroll', function () {

          var scrollTop = $(window).scrollTop();

          if (scrollTop >= topmenu_top) {
            topmenu.addClass('-fixed');
          } else {
            topmenu.removeClass('-fixed');
          }

        });
      }

    });

    $(function () {

      $('.b-testim__counter-value').each(function () {

        var $this = $(this),
          value_count = $this.text();

        value_count = Number(value_count.replace(/[+]/, ''));

        if (value_count > 0) {
          $this.addClass('-plus');
        } else if (value_count < 0) {
          $this.addClass('-minus');
        }

      });

      $('.b-testim__counter-up').on('click', function () {

        var $this = $(this),
          counter = $this.closest('.b-testim__counter'),
          down = counter.find('.b-testim__counter-down'),
          value = counter.find('.b-testim__counter-value'),
          value_count = value.text();

        value_count = Number(value_count.replace(/[+]/, ''));

        if (counter.hasClass('-default')) {
          counter.removeClass('-default');
          counter.addClass('-positive');
          $this.addClass('-active');

          value_count = value_count + 1;

          if (value_count > 0) {
            value.removeClass('-minus');
            value.addClass('-plus');
            value_count = '+' + value_count
          } else if (value_count < 0) {
            value.removeClass('-plus');
            value.addClass('-minus');
          } else {
            value.removeClass('-plus');
            value.removeClass('-minus');
          }
          value.text(value_count);

        } else if (counter.hasClass('-negative')) {
          down.removeClass('-active');
          counter.removeClass('-negative');
          counter.addClass('-default');
          value_count = value_count + 1;

          if (value_count > 0) {
            value.removeClass('-minus');
            value.addClass('-plus');
            value_count = '+' + value_count
          } else if (value_count < 0) {
            value.removeClass('-plus');
            value.addClass('-minus');
          } else {
            value.removeClass('-plus');
            value.removeClass('-minus');
          }
          value.text(value_count);

        } else if (counter.hasClass('-positive')) {
          return
        }

      });

      $('.b-testim__counter-down').on('click', function () {

        var $this = $(this),
          counter = $this.closest('.b-testim__counter'),
          up = counter.find('.b-testim__counter-up'),
          value = counter.find('.b-testim__counter-value'),
          value_count = value.text();

        value_count = Number(value_count.replace(/[+ ]/, ''));

        if (counter.hasClass('-default')) {
          counter.removeClass('-default');
          counter.addClass('-negative');
          $this.addClass('-active');
          value_count = value_count - 1;

          if (value_count > 0) {
            value.removeClass('-minus');
            value.addClass('-plus');
            value_count = '+' + value_count
          } else if (value_count < 0) {
            value.removeClass('-plus');
            value.addClass('-minus');
          } else {
            value.removeClass('-plus');
            value.removeClass('-minus');
          }

          value.text(value_count);
        } else if (counter.hasClass('-positive')) {
          up.removeClass('-active');
          counter.removeClass('-positive');
          counter.addClass('-default');
          value_count = value_count - 1;

          if (value_count > 0) {
            value.removeClass('-minus');
            value.addClass('-plus');
            value_count = '+' + value_count
          } else if (value_count < 0) {
            value.removeClass('-plus');
            value.addClass('-minus');
          } else {
            value.removeClass('-plus');
            value.removeClass('-minus');
          }

          value.text(value_count);
        } else if (counter.hasClass('-negative')) {
          return
        }

      });

    });

    $('.e-reprew__toggle').on('click', function () {

      var $this = $(this),
        dropdown = $this.siblings('.e-reprew__dropdown');

      dropdown.toggleClass('-active');

      $(document).on('click', function (e) {

        if (!$(e.target).closest('.e-reprew').length) {
          dropdown.removeClass('-active');
        }

      });
    });

    $('.b-topmenu__list').on('click', 'a', function (e) {
      e.preventDefault();

      var elementScroll = $(this).attr('href').replace(/\//gi, '');

      $('html, body').animate({
        scrollTop: $(elementScroll).offset().top - 100
      }, 800);

    });

  },

  upload: function () {

    // var file = $('input[type=file]'),
    //   upload = file.closest('.b-addrev__upload'),
    //   text = upload.find('.b-addrev__upload-text');
    //
    // file.fileupload({
    //   dataType: 'json',
    //   done: function (e, data) {
    //     $.each(data.result.files, function (index, file) {
    //       upload.append($('<span class=".b-addrev__upload-filename"></span>').text(file.name));
    //     });
    //   }
    // });

  },

  see_more: function () {

    $('.jsSeeMore__btn').on('click', function () {

      var $this = $(this),
        container = $this.closest('.jsSeeMore'),
        desc = container.find('.jsSeeMore__desc');

      if ($(this).hasClass('-open')) {
        desc.css('max-height', '');
        $this.text('see more');
        $this.removeClass('-open');
      } else {
        desc.css('max-height', 'none');
        $this.text('see less');
        $this.addClass('-open');
      }

    });

  },

  testim: function () {

    $('.b-testim__desc').each(function () {

      var $this = $(this),
        hidden = $this.find('.b-testim__desc-hidden'),
        text = $this.find('.b-testim__desc-text');

      if (text.height() > hidden.height()) {

        if ($this.find('.b-testim__desc-more').length) {
          return
        }

        $this
          .addClass('-more')
          .append('<button class="b-testim__desc-more">see more</button>');
      } else {
        $this
          .removeClass('-more')
          .find('.b-testim__desc-more').remove();
      }

    });

    $('.b-testim__desc').on('click', '.b-testim__desc-more', function () {

      var $this = $(this),
        desc = $this.closest('.b-testim__desc'),
        text = desc.find('.b-testim__desc-text');

      text.css('max-height', 'none');
      $this.remove();

    });

  },

  comments: function () {

    $('.b-comments__desc').each(function () {

      var $this = $(this),
        hidden = $this.find('.b-comments__desc-hidden'),
        text = $this.find('.b-comments__desc-text');

      if (text.height() > hidden.height()) {

        if ($this.find('.b-comments__desc-more').length) {
          return
        }

        $this
          .addClass('-more')
          .append('<button class="b-comments__desc-more">see more</button>');
      } else {
        $this
          .removeClass('-more')
          .find('.b-comments__desc-more').remove();
      }

    });

    $('.b-comments__desc').on('click', '.b-comments__desc-more', function () {

      var $this = $(this),
        desc = $this.closest('.b-comments__desc'),
        text = desc.find('.b-comments__desc-text');

      text.css('max-height', 'none');
      $this.remove();

    });

  },

  tops: function () {

    $('.b-tops__more-link').on('click', function (e) {
      e.preventDefault();
      var $this = $(this),
        container = $this.closest('.b-tops__section'),
        item = container.find('.b-tops__item');

      item.each(function () {
        $(this).addClass('-active');
      });

      if ($this.hasClass('-active')) {
        $this.removeClass('-active');
        $this.text('see more');
        item.removeClass('-fade');
        setTimeout(function () {
          item.removeClass('-active');
        }, 500);
      } else {
        $this.addClass('-active');
        $this.text('see less');
        item.addClass('-active');
        setTimeout(function () {
          item.addClass('-fade');
        }, 1);
      }

    });

  },

  fileUpload: function () {

    $('.jsFileInput').each(function (e) {

      var jsFileInput = $(this),
          input = jsFileInput.find('input[type="file"]');

      jsFileInput.append('<div class="b-form__file-list"></div>');

      var list = jsFileInput.find('.b-form__file-list');

      input.on('change', function (evt) {

        list.empty();

        var files = evt.target.files;

        for (var i = 0, f; f = files[i]; i++) {

          if (!f.type.match('image.*')) {
            continue;
          }

          var reader = new FileReader();

          reader.onload = (function(theFile) {
            return function(e) {

              var span = $('<span/>');

              span.addClass('b-form__file-item').append('<a class="b-form__file-img" style="background-image: url(' + e.target.result + ')" href="' + e.target.result + '"  title="' + theFile.name + '"></a>');
              list.append(span);
            };
          })(f);

          reader.readAsDataURL(f);
        }

      });

      jsFileInput.magnificPopup({
        delegate: '.b-form__file-img',
        type: 'image',
        mainClass: 'mfp-fade',
        removalDelay: 300
      });

    });

  },

  makeacomp: function () {

    $('.b-trouble__select').on('click', 'a.b-trouble__select-link', function (e) {
      e.preventDefault();


      $('.b-trouble__select-dropdown').removeClass('-active');
      $('.b-makeacomp__help').slideDown();

    });

  },

  gameslist: function () {

    $('.b-gameslist').slick({
      slidesToShow: 14,
      responsive: [
        {
          breakpoint: 1630,
          settings: {
            slidesToShow: 10,
          }
        },
        {
          breakpoint: 1230,
          settings: {
            slidesToShow: 8,
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 5,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
          }
        }
      ]
    });

  },

  newyear: function () {

    $(function() {
      var d = function() {};
      $(document).delegate('.b-ball_bounce', 'mouseenter', function() {
        b(this);
        m(this)
      }).delegate('.b-ball_bounce .b-ball__right', 'mouseenter', function(i) {
        i.stopPropagation();
        b(this);
        m(this)
      });

      function f() {
        var i = 'ny2012.swf';
        i = i + '?nc=' + (new Date().getTime());
        swfobject.embedSWF(i, 'z-audio__player', '1', '1', '9.0.0', null, {}, {
          allowScriptAccess: 'always',
          hasPriority: 'true'
        })
      }

      function h(i) {
        if ($.browser.msie) {
          return window[i]
        } else {
          return document[i]
        }
      }
      window.flashInited = function() {
        d = function(j) {
          try {
            h('z-audio__player').playSound(j)
          } catch (i) {}
        }
      };
      if (window.swfobject) {
        window.setTimeout(function() {
          $('body').append('<div class="g-invisible"><div id="z-audio__player"></div></div>');
          f()
        }, 100)
      }
      var l = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';'];
      var k = ['z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/']
      var g = 36;
      var a = {};
      for (var e = 0, c = l.length; e < c; e++) {
        a[l[e].charCodeAt(0)] = e
      }
      for (var e = 0, c = k.length; e < c; e++) {
        a[k[e].charCodeAt(0)] = e
      }
      $(document).keypress(function(j) {
        var i = $(j.target);
        if (!i.is('input') && j.which in a) {
          d(a[j.which])
        }
      });

      function b(n) {
        if (n.className.indexOf('b-ball__right') > -1) {
          n = n.parentNode
        }
        var i = /b-ball_n(\d+)/.exec(n.className);
        var j = /b-head-decor__inner_n(\d+)/.exec(n.parentNode.className);
        if (i && j) {
          i = parseInt(i[1], 10) - 1;
          j = parseInt(j[1], 10) - 1;
          d((i + j * 9) % g)
        }
      }

      function m(j) {
        var i = $(j);
        if (j.className.indexOf(' bounce') > -1) {
          return
        }
        i.addClass('bounce');

        function n() {
          i.removeClass('bounce').addClass('bounce1');

          function o() {
            i.removeClass('bounce1').addClass('bounce2');

            function p() {
              i.removeClass('bounce2').addClass('bounce3');

              function q() {
                i.removeClass('bounce3')
              }
              setTimeout(q, 300)
            }
            setTimeout(p, 300)
          }
          setTimeout(o, 300)
        }
        setTimeout(n, 300)
      }
    });

  }

};

$(document).ready(app.init());